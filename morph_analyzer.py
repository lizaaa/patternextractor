# -*- coding: utf-8 -*-
import time
import dawg
import array
import pickle

print("Loading files...")
paradigm_file = open("parsed_dict/arr_paradigms.pickle", "rb")
prefs_file = open("parsed_dict/prefixes.pickle", "rb")
suffs_file = open("parsed_dict/suffixes.pickle", "rb")
tags_file = open("parsed_dict/tags.pickle", "rb")

all_prefixes = pickle.load(prefs_file)
all_suffixes = pickle.load(suffs_file)
all_tags = pickle.load(tags_file)
all_paradigms = []

while True:
    try:
        all_paradigms.append(pickle.load(paradigm_file))
    except EOFError:
        break

print("Loading dictionary...")
dawg_dict = dawg.RecordDAWG(">II")
dawg_dict.load('dict.dawg')
replaces = dawg.DAWG.compile_replaces({u'е': u'ё', u'ё': u'е'})

def get_stem(str, prefix, suffix):
    return str[str.startswith(prefix) and len(prefix):str.endswith(suffix) and len(str) - len(suffix)]

def lemmatize(word, paradigm, i):
    N = len(paradigm) // 3
    pref = all_prefixes[paradigm[i]]
    suff = all_suffixes[paradigm[N + i]]
    stem = get_stem(word, pref, suff)
    lemma_pref = all_prefixes[paradigm[0]]
    lemma_suff = all_suffixes[paradigm[N]]
    return lemma_pref + stem + lemma_suff

def get_wordform(paradigm, i):
    N = len(paradigm) // 3
    pref_i = i
    suff_i = N + i
    tag_i = N*2 + i
    return (paradigm[pref_i], paradigm[suff_i], paradigm[tag_i])

def analyze(word):
    # здесь была ошибка из-за регистра слов, теперь все в нижнем
    word = word.lower()
    similar_words = dawg_dict.similar_keys(word, replaces)
    if similar_words == []:
        return ["UnknownWord"]
    else:
        parse_vars_pretty = []
        for word in similar_words:
            parse_vars = dawg_dict[word]
            for parse_v in parse_vars:
                par_i = parse_v[0]
                form_i = parse_v[1]
                _, _, tag_i = get_wordform(all_paradigms[par_i], form_i)
                # здесь была ошибка, поэтому для леммы возвращался []
                #if form_i == 0:
                    # continue
                if form_i != 0:
                    lemma = lemmatize(word, all_paradigms[par_i], form_i)
                else:
                    lemma = word
                _, _, lemma_tag = get_wordform(all_paradigms[par_i], 0)
                lemma_tag = all_tags[lemma_tag]
                if form_i != 0:
                    tag = lemma_tag+" "+all_tags[tag_i]
                else:
                    tag = lemma_tag
                parse_vars_pretty.append({"lemma": lemma, "tag": tag})
        return parse_vars_pretty