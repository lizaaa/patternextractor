# -*- coding: utf-8 -*-
from nltk.tokenize import word_tokenize
import os
import pickle
#import morph_analyzer as morph
import string
from collections import defaultdict

in_f = open("banned_symbs.pickle", "rb")
banned_symbs = pickle.load(in_f)
in_f.close()

def collect_banned_symbs(source_dir="corpus", files_N=1000):
    """
    Функция собирает мусорные символы в словах из конкорданса.
    Мусорные символы - знаки ударения, пунктуации и т.д.
    
    source_dir - путь к конкордансу
    files_N - кол-во тектсов в нем
    
    Собранные символы (множество) возвращаются и сериализуются в файл.
    """
    banned_symbs = set()
    
    for i in range(files_N):
        in_f = open(os.path.join(source_dir, str(i)+".txt"), 'r', encoding="utf-8")
        text = in_f.read()
        tokens = word_tokenize(text)
        # для каждого слова
        for token in tokens:
            # запоминаем все символы, что не являются буквами
            banned_symbs.update([symb for symb in token if not symb.isalpha()])
        in_f.close()
    
    out_f = open("banned_symbs.pickle", "wb")
    pickle.dump(banned_symbs, out_f)
    out_f.close()
    return banned_symbs

def clean_word(word, dirt):
    """
    Функция очистки слова.

    word - само слово
    dirt - символы, которые должны быть удалены из него

    Возвращает очищенное слово.
    """
    # разрешаем двойников дефиса
    allow = ["—", "−", "–", "-"]
    # удаляем забаненые символы из слов
    for symb in dirt:
        if symb in word:
            if symb in allow:
                # удаляем двойников дефиса, только если они на краю слова
                if len(word) > 0 and word[0] == symb: word = word[1:]
                if len(word) > 0 and word[-1] == symb: word = word[:-1]
            else:
                word = word.replace(symb, '')
    return word

def preprocess_text(text):
    """
    Функция предобработки текста
    (как текста в корпусе, так и фразы, для которой строится конкорданс):
    1) токенизация
    2) очистка
    4) перевод в нижний регистр
    Возвращает очищенный текст.
    """
    # парсим текст --> слова и знаки препинания
    tokens = word_tokenize(text)
    # убираем пункт.знаки
    tokens = list(filter(lambda x: x not in string.punctuation, tokens))
    for i in range(len(tokens)):
        # чистим слово
        tokens[i] = clean_word(tokens[i], banned_symbs)
    return tokens
#collect_banned_symbs()