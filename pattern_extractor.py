import json
from utils import preprocess_text
from morph_analyzer import analyze
from itertools import *

# {id: {count: "", chunks: []}}
extracted = dict()

def parse_tags(all_tags):
    lemmas = list(map(lambda var: var["lemma"], all_tags))
    tags = list(map(lambda var: var["tag"], all_tags))
    tags = [t.replace(',', '').split() for t in tags]
    poss = [t[0] for t in tags]
    tags = [t[1:] for t in tags]
    return lemmas, poss, tags

def is_match(all_tags, p_pos, p_tags):
    all_tags = list(map(lambda var: var["tag"], all_tags))
    p_tags = set(p_tags.split())
    # идем по всем разборам токена
    for tags in all_tags:
        # парсим разбор в виде строки
        tags = tags.replace(',', '').split()
        if len(tags) == 0:
            return False
        # сохраняем часть речи
        pos = tags[0]
        # и остальные тэги
        tags = set(tags[1:])
        # не проверяем часть речи, если задано само слово в шаблоне
        if p_pos != None:
            # сразу переходим к другому разбору, если часть речи не та
            if pos != p_pos:
                continue
        # переходим к другому разбору, 
        # если все тэги шаблона не содержатся в разборе
        if not p_tags <= tags:
            continue
        else:
            # останавливаемся, как только найдем подходящий разбор, 
            # т.е. совпадающий с шаблоном
            return True
    return False

def update_extracted(p_id, chunk):
    if p_id in extracted.keys():
        extracted[p_id]["count"] += 1
        extracted[p_id]["chunks"].add(' '.join(chunk))
    else:
        extracted[p_id] = dict()
        extracted[p_id]["count"] = 1
        extracted[p_id]["chunks"] = set([' '.join(chunk)])

def extract_from_text_(text, patterns):
    # проходимся по шаблонам
    for p in patterns:
        # id и токены шаблона
        p_id = p["id"]
        tokens = p["pattern"]
        # кол-во токенов в шаблоне
        tokens_n = len(tokens)
        # избегаем index out of range
        end = len(text) - tokens_n + 1
        # проходимся по токенам в тексте
        for i in range(end):
            # берем фрагмент соответствующей длины
            chunk = text[i:i+tokens_n]
            # степень соответсвия шаблону (все ли токены фрагмента совпали)
            match_count = 0
            # проходимся по токенам во фрагменте
            for j in range(tokens_n):
                # все вар-ты разбра токена
                all_tags = analyze(chunk[j])
                if "UnknownWord" in all_tags:
                    break
                all_tags = list(map(lambda var: var["tag"], all_tags))
                # проверяем совпадает ли токен фрагмента с соотв. токеном шаблона
                if is_match(all_tags, tokens[j]["pos"], tokens[j]["tags"]):
                    match_count += 1
                else:
                    break
            # если все токены фрагмента и шаблона совпали
            if match_count == tokens_n:
                # сохраняем фрагмент
                update_extracted(p_id, chunk)

def calc_indexes(start_i, gap):
    inds = [start_i]
    for x in gap:
        inds.append(inds[-1] + x + 1)
    return inds

def is_form(all_tags, pattern_word):
    all_lemmas = list(map(lambda var: var["lemma"], all_tags))
    return True if pattern_word in all_lemmas else False

def check_match(tokens, chunk, j, all_tags):
    checked_pattern = tokens[j]
    checked_chunk = chunk[j]
    if "word" in checked_pattern.keys():
        if "all_forms" in checked_pattern.keys():
            return is_form(all_tags, checked_pattern["word"])
        else:
            return checked_pattern["word"] == checked_chunk
    else:
        return is_match(all_tags, checked_pattern["pos"], checked_pattern["tags"])

def extract_from_text(text, patterns):
    for p in patterns:
        p_id = p["id"]
        tokens = p["pattern"]
        tokens_n = len(tokens)
        gap = int(p["gap"])
        gaps = [x for x in product(range(0, gap+1), repeat=tokens_n-1)]
        for gap in gaps:
            for i in range(len(text)):
                inds = calc_indexes(i, gap)
                try:
                    chunk = [text[ind] for ind in inds]
                    match_count = 0
                    for j in range(tokens_n):
                        all_tags = analyze(chunk[j])
                        if "UnknownWord" in all_tags:
                            break
                        if check_match(tokens, chunk, j, all_tags):
                            match_count += 1
                        else:
                            break
                    if match_count == tokens_n:
                        update_extracted(p_id, chunk)
                except IndexError as error:
                    break


def write_to_file(out_file):
    for p_id, item in extracted.items():
        out_file.write("<pattern: " + p_id + ", " + "count: " + str(item["count"]) + ">\n")
        for chunk in item["chunks"]:
            out_file.write(chunk)
            out_file.write("\n")

def preprocess_patterns(patterns):
    for i, pattern in enumerate(patterns):
        tokens = pattern["pattern"]
        tokens = sorted(tokens, key=lambda token: token["position"])
        for j, token in enumerate(tokens):
            if "aligned" in token.keys():
                ind = int(token["aligned"]) - 1
                main_token = tokens[ind]
                if "word" in main_token:
                    if main_token["all_forms"] == "0":
                        _, pos, tags = parse_tags(analyze(main_token["word"]))
                        main_token["pos"] = pos[0]
                        main_token["tags"] = tags[0]
                        tokens[ind] = main_token
                    else:
                        main_token["pos"] = None
                        main_token["tags"] = None
                        tokens[ind] = main_token
                token["tags"] = main_token["tags"]
                tokens[j] = token
            else:
                continue
        patterns[i]["pattern"] = tokens
    return patterns

def extract_patterns(patterns_file="tmp.json", source="corpus", source_n=1000, out_file="result.txt"):
    
    patterns = json.load(open(patterns_file, 'r', encoding="utf-8"))
    patterns = preprocess_patterns(patterns)
    for p in patterns:
        print(p)
        print("---------------------")
    input()

    out_file = open(out_file, "w")

    for i in range(source_n):
        text_path = source+"/{}.txt".format(i)
        text_file = open(text_path, 'r', encoding="utf-8")
        text = preprocess_text(text_file.read())

        extract_from_text(text, patterns)
    write_to_file(out_file)

# toy test
# extract_patterns("tmp.json", "toy_corpus", 2)
# print(extracted)

# corpus test
# extract_patterns("tmp.json", "corpus", 1000)
#extract_patterns("patterns_word.json", "toy_corpus", 2)